package com.vazhasapp.tbcshopapp.extensions

fun String.isValid(): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}