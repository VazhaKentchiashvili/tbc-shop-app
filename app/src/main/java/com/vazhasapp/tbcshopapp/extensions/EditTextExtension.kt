package com.vazhasapp.tbcshopapp.extensions

import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.hideError() {
    error = null
}

fun TextInputLayout.showError(errorMessage: String) {
    error = errorMessage
}

fun TextInputLayout.hideEndIcon() {
    isEndIconVisible = false
}