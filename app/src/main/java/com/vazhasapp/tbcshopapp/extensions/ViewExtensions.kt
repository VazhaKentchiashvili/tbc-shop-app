package com.vazhasapp.tbcshopapp.extensions

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log.d
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import com.google.android.material.textfield.TextInputLayout
import com.vazhasapp.tbcshopapp.R

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.viewGoneIf(status: Boolean) {
    if (status)
        gone()
    else
        show()

}

fun SwipeRefreshLayout.swipeRefreshShow() {
    isRefreshing = true
}
fun SwipeRefreshLayout.swipeRefreshHide() {
    isRefreshing = false
}

fun Context.makeToast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun TextView.coloredText(string: Array<String>, color: Array<Int>) {
    val spannable = SpannableString(string.joinToString(""))

    var startIndex = 0
    for (i in string.indices) {
        spannable.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, color[i])),
            startIndex, startIndex + string[i].length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        startIndex += string[i].length
    }
    text = spannable
}