package com.vazhasapp.tbcshopapp.extensions

import android.app.Dialog
import android.view.Window
import android.view.WindowManager

fun Dialog.setUp(view: Int){
    window!!.requestFeature(Window.FEATURE_NO_TITLE)
    window!!.setBackgroundDrawableResource(android.R.color.transparent)
    setContentView(view)
    window!!.attributes.width = WindowManager.LayoutParams.MATCH_PARENT
    window!!.attributes.height = WindowManager.LayoutParams.WRAP_CONTENT
}