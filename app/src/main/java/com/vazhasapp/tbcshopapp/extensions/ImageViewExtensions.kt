package com.vazhasapp.tbcshopapp.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.vazhasapp.tbcshopapp.R

fun ImageView.setImageWithGlide(imageUrl: String) {
    Glide.with(this)
        .load(imageUrl)
        .placeholder(R.mipmap.ic_launcher)
        .into(this)
}