package com.vazhasapp.tbcshopapp.post

import com.vazhasapp.tbcshopapp.model.Post
import javax.inject.Inject

class PostRemoImpl @Inject constructor(private val postsUseCase: PostsUseCase) : PostRepo {

    override suspend fun getPosts() = postsUseCase.getPosts()

}