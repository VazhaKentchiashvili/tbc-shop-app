package com.vazhasapp.tbcshopapp.post

import com.vazhasapp.tbcshopapp.model.Post
import com.vazhasapp.tbcshopapp.networking.AuthUseCase

interface PostRepo {

    suspend fun getPosts(): PostsUseCase.ResponseHandler<List<Post>>

}