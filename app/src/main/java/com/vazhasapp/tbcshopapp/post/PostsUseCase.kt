package com.vazhasapp.tbcshopapp.post

import com.google.gson.Gson
import com.vazhasapp.tbcshopapp.model.LoginModel
import com.vazhasapp.tbcshopapp.model.Post
import com.vazhasapp.tbcshopapp.model.ServiceError
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class PostsUseCase @Inject constructor(private val postApiService: PostApiService) {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    sealed class ResponseHandler<out T> {
        data class Success<T>(val data: T?) : ResponseHandler<T>()
        data class Failure<T>(val errorMessage: String,val data: T? = null) : ResponseHandler<T>()
        object Loading : ResponseHandler<Nothing>()
    }

    suspend fun getPosts(): ResponseHandler<List<Post>> =
        withContext(ioDispatcher) {
            val result = postApiService.getAllPosts()
            ResponseHandler.Loading

            try {
                if (result.isSuccessful) {
                    val body = result.body()!!
                    ResponseHandler.Success(body)
                } else {
                    val serverError = result.errorBody().toString()
                  //  val serverErrorModel = Gson().fromJson(serverError, ServiceError::class.java)
                    ResponseHandler.Failure("Failed")
                }
            } catch (e: Exception) {
                if (e !is CancellationException) {
//                    val serverError = result.errorBody().toString()
//                    val serverErrorModel = Gson().fromJson(serverError, ServiceError::class.java)
                    ResponseHandler.Failure("Failed request")
                } else {
                    throw e
                }
            }
        }

}