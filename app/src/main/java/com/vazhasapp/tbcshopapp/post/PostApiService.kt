package com.vazhasapp.tbcshopapp.post

import com.vazhasapp.tbcshopapp.model.Post
import com.vazhasapp.tbcshopapp.utils.Constants.POSTS_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET

interface PostApiService {

    @GET(POSTS_ENDPOINT)
    suspend fun getAllPosts(): Response<List<Post>>

    //suspend fun createNewPost(): Response<>
}