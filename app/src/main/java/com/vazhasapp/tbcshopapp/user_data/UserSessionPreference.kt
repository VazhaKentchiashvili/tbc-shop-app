package com.vazhasapp.tbcshopapp.user_data

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserSessionPreference @Inject constructor(@ApplicationContext context: Context) {

    companion object {
        const val SESSION = "SESSION"
        const val TOKEN = "TOKEN"
        const val USER_ID = "USER_ID"
    }

    private val sharedPreference: SharedPreferences by lazy {
        context.getSharedPreferences("user", Context.MODE_PRIVATE)
    }

    fun saveUserSession(status: Boolean) = sharedPreference.edit().putBoolean(SESSION, status).apply()


    fun readUserSession() = sharedPreference.getBoolean(SESSION, false)


    fun saveToken(status: String) {
        sharedPreference.edit().putString(TOKEN, status).apply()
    }

    fun readToken() = sharedPreference.getString(TOKEN, " ")

    fun saveUserId(userId: Int) {
        sharedPreference.edit().putInt(USER_ID, userId).apply()
    }

    fun readUserId() = sharedPreference.getInt(USER_ID, -1)

}