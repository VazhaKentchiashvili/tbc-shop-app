package com.vazhasapp.tbcshopapp.screen.auth.signup

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vazhasapp.tbcshopapp.R
import com.vazhasapp.tbcshopapp.databinding.FragmentSignupBinding
import com.vazhasapp.tbcshopapp.extensions.*
import com.vazhasapp.tbcshopapp.networking.AuthUseCase
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignupFragment : BaseFragment<FragmentSignupBinding>(FragmentSignupBinding::inflate) {

    private val signupViewModel: SignupViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {

        binding.tvLogin.coloredText(
            arrayOf(
                getString(
                    R.string.alread_a_member
                ),
                getString(R.string.log_in)
            ), arrayOf(R.color.main_grey, R.color.hint_color)
        )

        binding.btnSignup.root.setText(R.string.sign_up)

        binding.btnSignup.root.setOnClickListener {
            checkEnteredSignupInfo()
        }

        binding.etEmailAddress.doOnTextChanged { text, _, _, _ ->
            isEmailValid(text.toString())
            binding.tilEmailAddress.hideError()

            if (text.isNullOrBlank()) {
                binding.tilEmailAddress.hideEndIcon()
            }
        }

        observer()
    }

    private fun observer() {
        signupViewModel.userSignup.observe(viewLifecycleOwner, {
            when (it) {
                is AuthUseCase.ResponseHandler.Failure -> errorDialog(it.errorMessage)
                AuthUseCase.ResponseHandler.Loading -> binding.pbLoading.show()
                is AuthUseCase.ResponseHandler.Success -> requireContext().makeToast("Success registered")
            }
        })
    }

    private fun checkEnteredSignupInfo() {
        val email = binding.etEmailAddress.text.toString()
        val password = binding.etPassword.text.toString()
        val fullName = binding.etFullName.text.toString()

        if (!email.isNullOrBlank() && !password.isNullOrBlank() && !fullName.isNullOrBlank()) {
            if (email.isValid()) {
                if (password != binding.etRepeatPassword.text.toString()) {
                    binding.tilRepeatPassword.showError("Password doesn't match")
                } else {
                    signupViewModel.signup(email, password, fullName)
                }
            } else {
                binding.tilEmailAddress.showError("Please enter correct email")
            }
        } else {
            with(binding) {
                tilEmailAddress.showError("Please fill required fields")
                tilPassword.showError("Please fill required fields")
                tilRepeatPassword.showError("Please fill required fields")
                tilFullName.showError("Please fill required fields")
            }
        }
    }

    private fun errorDialog(errorDesc: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.custom_dialog_view)
        dialog.findViewById<TextView>(R.id.tvErrorMessage).text = errorDesc
        dialog.findViewById<Button>(R.id.btnClose).setOnClickListener { dialog.cancel() }
        dialog.show()
    }

    private fun isEmailValid(email: String) {
        binding.tilEmailAddress.isEndIconVisible = email.isValid()
    }
}