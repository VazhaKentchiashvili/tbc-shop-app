package com.vazhasapp.tbcshopapp.screen.fragments

import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.vazhasapp.tbcshopapp.R
import com.vazhasapp.tbcshopapp.adapters.CustomDrawerAdapter
import com.vazhasapp.tbcshopapp.databinding.FragmentHomeBinding
import com.vazhasapp.tbcshopapp.model.DrawerItem
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private lateinit var navHostFragment: NavHostFragment

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()
    }

    private fun init() {
        navHostFragment =
            childFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment

        binding.bottomNav.setupWithNavController(navHostFragment.navController)

        setupDrawerRecycler()
        setupDrawerMenu()
    }

    private fun setupDrawerRecycler() {
        val drawerMenuAdapter = CustomDrawerAdapter(
            arrayOf(
                DrawerItem(R.id.action_loginFragment_to_singupFragment, getString(R.string.my_profile)),
                DrawerItem(R.id.action_loginFragment_to_singupFragment, getString(R.string.my_post)),
                DrawerItem(R.id.action_loginFragment_to_singupFragment, getString(R.string.about)),
                DrawerItem(R.id.action_loginFragment_to_singupFragment, getString(R.string.my_cart)),
            )
        )
        drawerMenuAdapter.clicker = {
            binding.drawerLayout.closeDrawer(GravityCompat.END)
            val host = requireActivity().supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
            host.navController.navigate(R.id.action_global_loginFragment)
        }

        binding.rvCustomDrawer.apply {
            adapter = drawerMenuAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }

    private fun setupDrawerMenu() {

        val navView: NavigationView = binding.customNavView

        navView.setupWithNavController(navHostFragment.navController)

        binding.customToolbar.setupWithNavController(navHostFragment.navController)
    }
}