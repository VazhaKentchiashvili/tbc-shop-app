package com.vazhasapp.tbcshopapp.screen.favorites

import android.view.LayoutInflater
import android.view.ViewGroup
import com.vazhasapp.tbcshopapp.databinding.FavoritesFragmentBinding
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment

class FavoritesFragment : BaseFragment<FavoritesFragmentBinding>(FavoritesFragmentBinding::inflate) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

    }
}