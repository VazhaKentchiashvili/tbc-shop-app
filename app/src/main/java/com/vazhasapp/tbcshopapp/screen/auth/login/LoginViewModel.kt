package com.vazhasapp.tbcshopapp.screen.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.tbcshopapp.model.LoginModel
import com.vazhasapp.tbcshopapp.model.UserIdStatus
import com.vazhasapp.tbcshopapp.networking.AuthRepoImpl
import com.vazhasapp.tbcshopapp.networking.AuthUseCase
import com.vazhasapp.tbcshopapp.user_data.UserSessionPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authRepoImpl: AuthRepoImpl,
    private val userSessionPreference: UserSessionPreference
) : ViewModel() {
    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main

    private var _userLogin = MutableLiveData<AuthUseCase.ResponseHandler<LoginModel>>()
    val userLogin: LiveData<AuthUseCase.ResponseHandler<LoginModel>> get() = _userLogin

    private var _userCompleteStatus = MutableLiveData<AuthUseCase.ResponseHandler<UserIdStatus>>()
    val userCompleteStatus: LiveData<AuthUseCase.ResponseHandler<UserIdStatus>> get() = _userCompleteStatus

    fun login(email: String, password: String, rememberMe: Boolean) {
        viewModelScope.launch {
            withContext(mainDispatcher) {
                val response = authRepoImpl.loginRequest(email, password, rememberMe)
                _userLogin.postValue(response)
            }
        }
    }

    fun profileStatus() {
        viewModelScope.launch {
            withContext(mainDispatcher) {
                val response = authRepoImpl.profileStatus(userSessionPreference.readUserId())
                _userCompleteStatus.postValue(response)
            }
        }
    }
}