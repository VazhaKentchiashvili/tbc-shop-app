package com.vazhasapp.tbcshopapp.screen.complete_profile

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.vazhasapp.tbcshopapp.databinding.ProfileCompleteFragmentBinding
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment

class ProfileCompleteFragment :
    BaseFragment<ProfileCompleteFragmentBinding>(ProfileCompleteFragmentBinding::inflate), OnMapReadyCallback{

    private val viewModel: ProfileCompleteViewModel by viewModels()
    private lateinit var map: GoogleMap

    private val request =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {

            if (it[Manifest.permission.CAMERA] == true &&
                it[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true &&
                it[Manifest.permission.READ_EXTERNAL_STORAGE] == true
            ) {
                dispatchTakePictureIntent()
            } else {
                Toast.makeText(requireContext(), "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.btnCompleteProfile.root.setOnClickListener() {
            permissionsRequest(request)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        var home = LatLng(41.753911, 44.768111)

        map = googleMap
        map.addMarker(
            MarkerOptions().position(home).title("Aqa vaart chemi kaiebi")
        )
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(home, 18F))

        map.addMarker(
            MarkerOptions().position(home)
                .title("")
        )
    }

    private val resultContract =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {

            if (it.resultCode == Activity.RESULT_OK) {
                val imageBitmap = it.data?.extras?.get("data") as Bitmap
                binding.imUploadPicture.setImageBitmap(imageBitmap)
            }
        }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            resultContract.launch(takePictureIntent)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }

}