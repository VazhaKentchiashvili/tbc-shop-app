package com.vazhasapp.tbcshopapp.screen.splash

import androidx.lifecycle.ViewModel
import com.vazhasapp.tbcshopapp.user_data.UserSessionPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModel @Inject constructor(private val userSessionPreference: UserSessionPreference) :
    ViewModel() {

    fun isSessionSaved() = userSessionPreference.readUserSession()
}