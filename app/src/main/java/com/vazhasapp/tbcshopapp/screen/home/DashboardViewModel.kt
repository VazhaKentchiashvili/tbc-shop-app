package com.vazhasapp.tbcshopapp.screen.home

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.tbcshopapp.model.Post
import com.vazhasapp.tbcshopapp.networking.AuthRepoImpl
import com.vazhasapp.tbcshopapp.post.PostRemoImpl
import com.vazhasapp.tbcshopapp.post.PostsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(private val postRepoImpl: PostRemoImpl) : ViewModel() {

    private val _postsMutableLiveData =
        MutableLiveData<PostsUseCase.ResponseHandler<List<Post>>>()

    val postsLiveData: LiveData<PostsUseCase.ResponseHandler<List<Post>>>
        get() =
            _postsMutableLiveData

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    fun initViewModel() {
        getPosts()
    }

    private fun getPosts() {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                val result = postRepoImpl.getPosts()
                _postsMutableLiveData.postValue(result)
                d("Result", "$result")
            }
        }
    }
}