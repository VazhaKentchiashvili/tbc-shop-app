package com.vazhasapp.tbcshopapp.screen.auth.login

import android.content.Context
import com.vazhasapp.tbcshopapp.extensions.isValid
import dagger.hilt.android.qualifiers.ApplicationContext

class AuthInfoCheckUseCase {

    fun checkEnteredLoginInfo(email: String, password: String): Boolean {

        return if (email.isNotBlank() && password.isNotBlank()) {
            email.isValid()
        } else {
            false
        }
    }

//    fun checkEnteredSignupInfo(email: String, password: String, fullName: String): Boolean {
//        return if (email.isNotBlank() && password.isNotBlank() && fullName.isNotBlank()) {
//            email.isValid()
//        } else {
//            false
//        }
//    }
}