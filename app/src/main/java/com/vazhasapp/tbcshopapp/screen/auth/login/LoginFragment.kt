package com.vazhasapp.tbcshopapp.screen.auth.login

import android.app.Dialog
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vazhasapp.tbcshopapp.R
import com.vazhasapp.tbcshopapp.databinding.FragmentLoginBinding
import com.vazhasapp.tbcshopapp.extensions.*
import com.vazhasapp.tbcshopapp.networking.AuthUseCase
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    private val loginViewModel: LoginViewModel by viewModels()

    @Inject lateinit var authInfoCheck: AuthInfoCheckUseCase

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {

        binding.tilEmailAddress.hideEndIcon()
        binding.btnLogin.root.setText(R.string.sign_in)

        binding.btnLogin.root.setOnClickListener {
            enteredInfoChecker()
        }
        listener()

        binding.tvSignUp.coloredText(
            arrayOf(
                getString(R.string.new_user),
                getString(R.string.sign_up),
                getString(R.string.here)
            ), arrayOf(R.color.main_grey, R.color.hint_color, R.color.main_grey)
        )

        binding.etEmailAddress.doOnTextChanged { text, _, _, _ ->
            isEmailValid(text.toString())
            binding.tilEmailAddress.hideError()

            if (text.isNullOrBlank()) {
                binding.tilEmailAddress.hideEndIcon()
            }
        }
    }

    private fun listener() {
        loginViewModel.userLogin.observe(viewLifecycleOwner, {
            when (it) {
                is AuthUseCase.ResponseHandler.Loading -> binding.pbLoading.show()

                is AuthUseCase.ResponseHandler.Success -> {
                    binding.pbLoading.hide()
                    requireContext().makeToast("Success login")
                    findNavController().navigate(R.id.action_loginFragment_to_homeFragment2)
                }
                
                is AuthUseCase.ResponseHandler.Failure -> {
                    binding.pbLoading.hide()
                    errorDialog(it.errorMessage)
                }
            }
        })

        loginViewModel.userCompleteStatus.observe(viewLifecycleOwner, {

            when (it) {
                is AuthUseCase.ResponseHandler.Loading -> binding.pbLoading.show()

                is AuthUseCase.ResponseHandler.Success -> {

                    if (it.data!!.status) {
                        findNavController().navigate(R.id.action_loginFragment_to_homeFragment2)

                    }else {
                        findNavController().navigate(R.id.action_loginFragment_to_profileCompleteFragment)
                    }

                }

                is AuthUseCase.ResponseHandler.Failure -> {
                    binding.pbLoading.hide()
                    errorDialog(it.errorMessage)
                }
            }

        })

    }

    private fun errorDialog(desc: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.custom_dialog_view)
        dialog.findViewById<TextView>(R.id.tvErrorMessage).text = desc
        dialog.findViewById<Button>(R.id.btnClose).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }

    private fun enteredInfoChecker() {
        val email = binding.etEmailAddress.text.toString()
        val password = binding.etPassword.text.toString()
        val session = binding.chbRememberMe.isChecked

        if (authInfoCheck.checkEnteredLoginInfo(email, password)) {
            loginViewModel.login(email, password, session)
        } else {
            binding.tilEmailAddress.showError("Something went wrong")
            binding.tilPassword.showError("Something went wrong")
        }
    }

    private fun isEmailValid(email: String) {
        binding.tilEmailAddress.isEndIconVisible = email.isValid()
    }
}