package com.vazhasapp.tbcshopapp.screen.my_wallet

import android.view.LayoutInflater
import android.view.ViewGroup
import com.vazhasapp.tbcshopapp.databinding.MyWalletFragmentBinding
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment

class MyWalletFragment : BaseFragment<MyWalletFragmentBinding>(MyWalletFragmentBinding::inflate) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

    }
}