package com.vazhasapp.tbcshopapp.screen.auth.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.tbcshopapp.model.SignupModel
import com.vazhasapp.tbcshopapp.networking.AuthRepoImpl
import com.vazhasapp.tbcshopapp.networking.AuthUseCase
import com.vazhasapp.tbcshopapp.user_data.UserSessionPreference
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignupViewModel @Inject constructor(
    private val authRepoImpl: AuthRepoImpl
) : ViewModel() {

    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main

    private var _userSignup = MutableLiveData<AuthUseCase.ResponseHandler<SignupModel>>()
    val userSignup: LiveData<AuthUseCase.ResponseHandler<SignupModel>> get() = _userSignup


    fun signup(email: String, password: String, fullName: String) {
        viewModelScope.launch {
            withContext(mainDispatcher) {
                val response = authRepoImpl.signupRequest(email, password, fullName)
                _userSignup.postValue(response)
            }
        }
    }
}