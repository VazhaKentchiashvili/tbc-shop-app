package com.vazhasapp.tbcshopapp.screen.splash

import android.animation.Animator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.Lottie
import com.airbnb.lottie.LottieAnimationView
import com.vazhasapp.tbcshopapp.R
import com.vazhasapp.tbcshopapp.databinding.FragmentSplashScreenBinding
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreenFragment :
    BaseFragment<FragmentSplashScreenBinding>(FragmentSplashScreenBinding::inflate) {

    private val splashViewModel: SplashScreenViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {

        binding.lotSplashAnimation.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                openFragment()
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }
        })
    }

    private fun openFragment() {
        if (splashViewModel.isSessionSaved())
            findNavController().navigate(R.id.action_splashScreenFragment_to_homeFragment2)
        else
            findNavController().navigate(R.id.action_splashScreenFragment_to_loginFragment)
    }
}