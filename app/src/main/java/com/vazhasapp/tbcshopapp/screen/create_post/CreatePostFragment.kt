package com.vazhasapp.tbcshopapp.screen.create_post


import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.vazhasapp.tbcshopapp.databinding.CreatePostFragmentBinding
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment
import java.util.*

class CreatePostFragment :
    BaseFragment<CreatePostFragmentBinding>(CreatePostFragmentBinding::inflate) {

    private val request =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {

            if (it[Manifest.permission.CAMERA] == true &&
                it[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true &&
                it[Manifest.permission.READ_EXTERNAL_STORAGE] == true
            ) {
                dispatchTakePictureIntent()
            } else {
                Toast.makeText(requireContext(), "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        binding.btnChoosePhoto.setOnClickListener {
            permissionsRequest(request)
        }
    }

    private val resultContract =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {

            if (it.resultCode == RESULT_OK) {
                val imageBitmap = it.data?.extras?.get("data") as Bitmap
                binding.imPhotos.setImageBitmap(imageBitmap)
            }
        }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            resultContract.launch(takePictureIntent)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }
}