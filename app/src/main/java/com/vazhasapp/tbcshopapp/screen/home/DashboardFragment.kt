package com.vazhasapp.tbcshopapp.screen.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.tbcshopapp.adapters.PostsRecyclerAdapter
import com.vazhasapp.tbcshopapp.databinding.DashboardFragmentBinding
import com.vazhasapp.tbcshopapp.extensions.swipeRefreshHide
import com.vazhasapp.tbcshopapp.extensions.swipeRefreshShow
import com.vazhasapp.tbcshopapp.post.PostsUseCase
import com.vazhasapp.tbcshopapp.screen.fragments.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment :
    BaseFragment<DashboardFragmentBinding>(DashboardFragmentBinding::inflate) {

    private val postsRecyclerAdapter = PostsRecyclerAdapter()

    private val dashboardViewModel: DashboardViewModel by viewModels()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()

    }

    private fun init() {
        binding.swipeRefresh.setOnRefreshListener {
            dashboardViewModel.initViewModel()
        }

        dashboardViewModel.initViewModel()
        setupRecyclerView()
        observer()
    }

    private fun observer() {
        dashboardViewModel.postsLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is PostsUseCase.ResponseHandler.Failure -> binding.swipeRefresh.swipeRefreshHide()
                PostsUseCase.ResponseHandler.Loading -> binding.swipeRefresh.swipeRefreshShow()
                is PostsUseCase.ResponseHandler.Success -> {
                    postsRecyclerAdapter.setData(it.data!!.toMutableList())
                    binding.swipeRefresh.swipeRefreshHide()
                }
            }
        })
    }

    private fun setupRecyclerView() {

        binding.rvPosts.apply {
            adapter = postsRecyclerAdapter
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }
    }
}