package com.vazhasapp.tbcshopapp.networking

import com.vazhasapp.tbcshopapp.model.UserIdStatus
import javax.inject.Inject

class AuthRepoImpl @Inject constructor(
    private val authUseCase: AuthUseCase
) : AuthRepo {

    override suspend fun loginRequest(
        email: String,
        password: String,
        rememberMe: Boolean
    ) = authUseCase.logIn(email, password, rememberMe)

    override suspend fun signupRequest(
        email: String,
        password: String,
        fullName: String
    ) = authUseCase.signUp(email, password, fullName)

    override suspend fun profileStatus(userId: Int) =
        authUseCase.profileStatus(userId)

}