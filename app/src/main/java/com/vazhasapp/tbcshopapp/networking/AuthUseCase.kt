package com.vazhasapp.tbcshopapp.networking

import com.google.gson.Gson
import com.vazhasapp.tbcshopapp.model.LoginModel
import com.vazhasapp.tbcshopapp.model.ServiceError
import com.vazhasapp.tbcshopapp.model.SignupModel
import com.vazhasapp.tbcshopapp.model.UserIdStatus
import com.vazhasapp.tbcshopapp.user_data.UserSessionPreference
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class AuthUseCase @Inject constructor(
    private val apiService: AuthApiService,
    private val userSessionPreference: UserSessionPreference
) {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    sealed class ResponseHandler<out T> {
        data class Success<T>(val data: T?) : ResponseHandler<T>()
        data class Failure<T>(val errorMessage: String, val data: T? = null) : ResponseHandler<T>()
        object Loading : ResponseHandler<Nothing>()
    }

    suspend fun logIn(
        email: String,
        password: String,
        rememberMe: Boolean
    ): ResponseHandler<LoginModel> =
        withContext(ioDispatcher) {
            val result = apiService.loginRequest(email, password)
            ResponseHandler.Loading
            try {
                if (result.isSuccessful) {
                    val body = result.body()!!
                    userSessionPreference.saveUserSession(rememberMe)
                    userSessionPreference.saveToken(body.token!!)
                    userSessionPreference.saveUserId(body.userId!!)
                    ResponseHandler.Success(body)
                } else {
                    val serverError = result.errorBody().toString()
                    val serverErrorModel = Gson().fromJson(serverError, ServiceError::class.java)
                    ResponseHandler.Failure(serverErrorModel.error.toString())
                }
            } catch (e: Exception) {
                if (e !is CancellationException) {
//                    val serverError = result.errorBody().toString()
//                    val serverErrorModel = Gson().fromJson(serverError, ServiceError::class.java)
                    ResponseHandler.Failure("Failed request")
                } else {
                    throw e
                }
            }
        }

    suspend fun signUp(
        email: String,
        password: String,
        fullName: String
    ): ResponseHandler<SignupModel> =
        withContext(ioDispatcher) {
            val result = apiService.signupRequest(email, password, fullName)

            try {
                if (result.isSuccessful) {
                    ResponseHandler.Success(result.body())
                } else {
                    ResponseHandler.Failure("Your request failed")
                }
            } catch (e: Exception) {
                if (e !is CancellationException) {
//                    val serverError = result.errorBody().toString()
//                    val serverErrorModel = Gson().fromJson(serverError, ServiceError::class.java)
                    ResponseHandler.Failure("Failed request")
                } else {
                    throw e
                }
            }
        }

    suspend fun profileStatus(userId: Int): ResponseHandler<UserIdStatus> =
        withContext(ioDispatcher) {
            val result = apiService.completeProfile(userId)

            try {
                if (result.isSuccessful) {
                    ResponseHandler.Success(result.body())
                } else {
                    ResponseHandler.Failure("Your request failed")
                }
            } catch (e: Exception) {
                if (e !is CancellationException) {
//                    val serverError = result.errorBody().toString()
//                    val serverErrorModel = Gson().fromJson(serverError, ServiceError::class.java)
                    ResponseHandler.Failure("Failed request")
                } else {
                    throw e
                }
            }
        }
}