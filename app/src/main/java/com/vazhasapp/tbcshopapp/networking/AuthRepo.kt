package com.vazhasapp.tbcshopapp.networking

import com.vazhasapp.tbcshopapp.model.LoginModel
import com.vazhasapp.tbcshopapp.model.SignupModel
import com.vazhasapp.tbcshopapp.model.UserIdStatus
import retrofit2.Response

interface AuthRepo {

    suspend fun loginRequest(
        email: String,
        password: String,
        rememberMe: Boolean
    ): AuthUseCase.ResponseHandler<LoginModel>

    suspend fun signupRequest(
        email: String,
        password: String,
        fullName: String
    ): AuthUseCase.ResponseHandler<SignupModel>

    suspend fun profileStatus(userId: Int): AuthUseCase.ResponseHandler<UserIdStatus>
}