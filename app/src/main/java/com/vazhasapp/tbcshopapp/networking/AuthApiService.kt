package com.vazhasapp.tbcshopapp.networking

import com.google.gson.annotations.SerializedName
import com.vazhasapp.tbcshopapp.model.LoginModel
import com.vazhasapp.tbcshopapp.model.SignupModel
import com.vazhasapp.tbcshopapp.model.UserIdStatus
import com.vazhasapp.tbcshopapp.utils.Constants.LOGIN_ENDPOINT
import com.vazhasapp.tbcshopapp.utils.Constants.PROFILE_ENDPOINT
import com.vazhasapp.tbcshopapp.utils.Constants.REGISTER_ENDPOINT
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthApiService {
    @POST(LOGIN_ENDPOINT)
    @FormUrlEncoded
    suspend fun loginRequest(
        @Field("email") email: String,
        @Field("password") password: String,
    ): Response<LoginModel>

    @POST(REGISTER_ENDPOINT)
    @FormUrlEncoded
    suspend fun signupRequest(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("full_name") fullName: String
    ): Response<SignupModel>

    @POST(PROFILE_ENDPOINT)
    @FormUrlEncoded
    suspend fun completeProfile(
        @Field("user_id") userId: Int
    ): Response<UserIdStatus>
}