package com.vazhasapp.tbcshopapp.di

import android.content.Context
import com.vazhasapp.tbcshopapp.BuildConfig
import com.vazhasapp.tbcshopapp.networking.AuthApiService
import com.vazhasapp.tbcshopapp.networking.AuthRepoImpl
import com.vazhasapp.tbcshopapp.networking.AuthUseCase
import com.vazhasapp.tbcshopapp.post.PostApiService
import com.vazhasapp.tbcshopapp.post.PostRemoImpl
import com.vazhasapp.tbcshopapp.post.PostsUseCase
import com.vazhasapp.tbcshopapp.screen.auth.login.AuthInfoCheckUseCase
import com.vazhasapp.tbcshopapp.user_data.UserSessionPreference
import com.vazhasapp.tbcshopapp.utils.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private fun okHttpClient(userPreference: UserSessionPreference): OkHttpClient {

        val builder = OkHttpClient.Builder().addInterceptor(Interceptor { chain ->
            chain.request().url

            val request = chain.request().newBuilder()

            val response = chain.proceed(request.build())

            val token = userPreference.readToken()

            if (!token.isNullOrBlank()) {
                request.addHeader("Authorization", "Bearer $token")
            }

            response
        })

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }

        return builder.build()
    }

    @Provides
    @Singleton
    fun authApiService(userPreference: UserSessionPreference): AuthApiService =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            //.client(okHttpClient(userPreference))
            .build().create(AuthApiService::class.java)

    @Provides
    @Singleton
    fun postApiService(userPreference: UserSessionPreference): PostApiService =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            //.client(okHttpClient(userPreference))
            .build().create(PostApiService::class.java)

    @Provides
    @Singleton
    fun authRepoImpl(
        authUseCase: AuthUseCase
    ): AuthRepoImpl = AuthRepoImpl(authUseCase)

    @Provides
    @Singleton
    fun authUseCase(
        apiService: AuthApiService,
        userSessionPreference: UserSessionPreference
    ): AuthUseCase = AuthUseCase(apiService, userSessionPreference)

    @Provides
    @Singleton
    fun postRepoImpl(postsUseCase: PostsUseCase): PostRemoImpl = PostRemoImpl(postsUseCase)

    @Provides
    @Singleton
    fun authInfoCheck(): AuthInfoCheckUseCase = AuthInfoCheckUseCase()
}