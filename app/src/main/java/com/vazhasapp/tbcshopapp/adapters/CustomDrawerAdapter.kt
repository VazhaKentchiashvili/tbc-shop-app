package com.vazhasapp.tbcshopapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.tbcshopapp.databinding.DrawerMenuItemBinding
import com.vazhasapp.tbcshopapp.model.DrawerItem

typealias Clicker = (position: Int) -> Unit

class CustomDrawerAdapter(
    val drawerItemList: Array<DrawerItem>
) : RecyclerView.Adapter<CustomDrawerAdapter.CustomDrawerViewHolder>() {


    lateinit var clicker: Clicker

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CustomDrawerViewHolder(
            DrawerMenuItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CustomDrawerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = drawerItemList.size

    inner class CustomDrawerViewHolder(private val binding: DrawerMenuItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var currentDrawerItem: DrawerItem
        fun bind() {
            currentDrawerItem = drawerItemList[adapterPosition]
            binding.root.text = currentDrawerItem.drawerMenuTitle
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            clicker(adapterPosition)
        }
    }
}