package com.vazhasapp.tbcshopapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.tbcshopapp.databinding.ViewPagerImageViewBinding
import com.vazhasapp.tbcshopapp.extensions.setImageWithGlide
import com.vazhasapp.tbcshopapp.model.Post

class PostsViewPagerAdapter : RecyclerView.Adapter<PostsViewPagerAdapter.PostsViewHolder>() {

    private val postsImageList = mutableListOf<Post.PostsImage>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PostsViewHolder(
            ViewPagerImageViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = postsImageList.size

    inner class PostsViewHolder(private val binding: ViewPagerImageViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
            private lateinit var currentPhotos: Post.PostsImage

            fun bind() {
                currentPhotos = postsImageList[adapterPosition]

                currentPhotos.url?.let { binding.postImage.setImageWithGlide(it) }

            }
    }

    fun setData(imageList: MutableList<Post.PostsImage>) {
        postsImageList.clear()
        postsImageList.addAll(imageList)
        notifyDataSetChanged()
    }

}