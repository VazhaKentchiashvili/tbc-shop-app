package com.vazhasapp.tbcshopapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.vazhasapp.tbcshopapp.R
import com.vazhasapp.tbcshopapp.databinding.PostsItemViewBinding
import com.vazhasapp.tbcshopapp.extensions.viewGoneIf
import com.vazhasapp.tbcshopapp.model.Post

class PostsRecyclerAdapter : RecyclerView.Adapter<PostsRecyclerAdapter.PostRecyclerViewHolder>() {

    private val postsList = mutableListOf<Post>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PostRecyclerViewHolder(
            PostsItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: PostRecyclerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = postsList.size

    inner class PostRecyclerViewHolder(private val binding: PostsItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
            private lateinit var currentPost: Post
            private lateinit var viewPageradapter: PostsViewPagerAdapter

            fun bind() {
                currentPost = postsList[adapterPosition]

                binding.tvPostTitle.text = currentPost.title
                binding.tvPostCategory.text = currentPost.tags
                binding.tvPostDescription.text = currentPost.description
                binding.tvPriceType.text = currentPost.priceType
                binding.tvPostPrice.text = currentPost.price.toString()
                binding.btnAddToCard.root.setText(R.string.add_to_card)

                initViewPagerAdapter()
            }
        private fun initViewPagerAdapter() {
            viewPageradapter = PostsViewPagerAdapter()
            currentPost.urls?.let { viewPageradapter.setData(it.toMutableList()) }
            binding.vpPostPhotos.adapter = viewPageradapter

            binding.vpPostPhotos.viewGoneIf(currentPost.urls.isNullOrEmpty())

            val photosAmount = currentPost.urls?.size ?: 0

            with(binding) {
                imSwipePhotoRight.viewGoneIf(photosAmount < 2)
                imSwipePhotoLeft.viewGoneIf(photosAmount < 2)
                tvPhotoCounter.viewGoneIf(photosAmount < 2)
            }
            if (photosAmount >= 2) {
                setPageIndexCounter(1)
            }
            binding.vpPostPhotos.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {

                override fun onPageSelected(position: Int) {
                    setPageIndexCounter(position + 1)
                }
            })
        }
        private fun setPageIndexCounter(page: Int) {
             binding.tvPhotoCounter.text = "$page ${currentPost.urls?.size}"
        }
    }

    fun setData(posts: MutableList<Post>) {
        postsList.clear()
        postsList.addAll(posts)
        notifyDataSetChanged()
    }
}