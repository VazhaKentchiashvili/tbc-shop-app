package com.vazhasapp.tbcshopapp.model

data class UserIdStatus(
    val completeStatus: Int,
    val status: Boolean,
)
