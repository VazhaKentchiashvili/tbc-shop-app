package com.vazhasapp.tbcshopapp.model

data class DrawerItem(
    val id: Int,
    val drawerMenuTitle: String,
)
