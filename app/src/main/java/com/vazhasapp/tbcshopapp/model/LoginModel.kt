package com.vazhasapp.tbcshopapp.model

import com.google.gson.annotations.SerializedName

data class LoginModel(

    @SerializedName("user_id")
    val userId: Int?,
    val token: String?,
)
