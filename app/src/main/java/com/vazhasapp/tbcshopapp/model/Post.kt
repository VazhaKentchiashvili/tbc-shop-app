package com.vazhasapp.tbcshopapp.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class Post(
    @SerializedName("category_id")
    val categoryId: Int?,
    val description: String?,
    val id: Int?,
    val owner: Int?,
    val price: Double?,
    @SerializedName("price_type")
    val priceType: String?,
    val tags: String?,
    val title: String?,
    val urls: List<PostsImage>?
){
    inner class PostsImage(
        val url: String?
    )
}