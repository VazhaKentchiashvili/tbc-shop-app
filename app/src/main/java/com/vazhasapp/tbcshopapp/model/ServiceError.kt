package com.vazhasapp.tbcshopapp.model

data class ServiceError(
    val OK: Boolean?,
    val error: String?,
)