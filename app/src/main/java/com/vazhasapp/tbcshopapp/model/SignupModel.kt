package com.vazhasapp.tbcshopapp.model

data class SignupModel(
    val ok: Boolean?,
    val registered: Boolean?,
)
