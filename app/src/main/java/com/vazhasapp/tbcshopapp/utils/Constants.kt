package com.vazhasapp.tbcshopapp.utils

object Constants {
    const val BASE_URL = "https://ktorhighsteaks.herokuapp.com/"
    const val LOGIN_ENDPOINT = "login"
    const val REGISTER_ENDPOINT = "register"
    const val POSTS_ENDPOINT = "posts"
    const val PROFILE_ENDPOINT = "profile"
}